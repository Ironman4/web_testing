<?php
  session_start() ;

 
?>


<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <style>
      #map{
        position:relative;
        height: 400px;
        width :100% ;
        margin-top: 10px;
        margin-bottom: 10px;
        
        
      }
      html, body {
        height: 100%;
        margin: 0;
        padding-top: 10px;
        text-align: center;
        margin-bottom: 100px ;
      }

        
    </style>
</head>
<body>
<h3  style="background-color: grey"> Choose your location by Picking up the marker and zooming it or search  </h3>
<div style="margin-left: 20% ;">
<form style="width: 50% ;position: relative;float: left;  box-shadow: 10px 10px 5px grey" >
    <input class="input-group type="text" name="location" placeholder="Location" id="location">
</form>
<button class="btn btn-primary" id="search" style="box-shadow: 10px 10px 5px grey; margin-left:5%" >search</button>

  </div>

 
 <div  >
    <div style="color: green">
      <div type="text" class="x" id="lat1"></div>
    </div >
    <div style="color: blue">
        <div type="text" class="y" id="lng1"></div>
      </div>
     
 </div>
<div id="zoom" class="z"></div>

<div class="z"></div>

<div id="map"  ></div>


  <button class="btn btn-success container-fluid" id="submit" style="box-shadow: 10px 10px 5px grey;margin-top: 4px ; margin-bottom: 4px; width: 40%" >Submit</button>



<!-- <a href="../addItem/">
  <button class="btn btn-primary"  style="box-shadow: 10px 10px 5px grey; margin-left:5% ;margin-top: 10px" >Add Item</button>
</a> -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  
   <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqwTVGM4GpQpReUhIl9iIY-EKAZB7tb9A&callback=initMap">
    </script>
<script>

      var marker =0;

    function initMap() {

      var address = 'x';


 
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15 
             
            
        });
 
        
        var geocoder = new google.maps.Geocoder();
 
        
        
        

         document.getElementById('search').addEventListener('click', function() {
          marker.setMap(null);
         
          
          geocodeAddress(geocoder, map );
        });


        function geocodeAddress(geocoder, resultsMap ,address) {
          

        if(address =='x'){
        var address = <?php  if (array_key_exists("city", $_SESSION)){ echo '"'. $_SESSION["city"] ." " . $_SESSION["state"] . " " . $_SESSION["country"] .'"' ;}else{ echo " ";}?>;
        }else{
           address = document.getElementById('location').value;
        }
        

        geocoder.geocode({'address': address}, function(results, status) {

          if (status === 'OK') {
            resultsMap.setCenter(results[0].geometry.location);
             marker = new google.maps.Marker({
              map: resultsMap,
              //pos is by default at right top
              position: results[0].geometry.location,
              draggable:true,
              title:"click on satellite mode to get exact location"

            });
             google.maps.event.addListener(
                marker,
                'dragend',
                function() {
                    document.getElementById('lat1').innerHTML = "Latitude :" + marker.position.lat().toFixed(8);
                    document.getElementById('lng1').innerHTML ="Longitude :" + marker.position.lng().toFixed(8);
                    document.getElementById('zoom').innerHTML = mapObject.getZoom();

                    // Dynamically show it somewhere if needed
                    $(".x").text( marker.position.lat().toFixed(8));
                    $(".y").text(marker.position.lng().toFixed(8));
                    $(".z").text(map.getZoom());


                }
            );      
             
                 
             
          } else {
            alert('Enter city state country correctly: eg. Lucknow Uttarpradesh India' );
          }
           
             
            });
        }


        document.getElementById('submit').addEventListener('click', function() {
              
                $.ajax({
                url: 'addLatLng.php',
                data: {"lat":marker.position.lat() ,'lng':marker.position.lng()},
                type: 'POST',
                success:function(data){
                    
                    alert("Location is set  ");
                     location.href = '../addItem/';
                  },
                  error:function(){
                    alert("location is not set")
                  }
                });


                


        });
        geocodeAddress(geocoder, map,address);

      /*
        google.maps.event.addListener(map, 'click', function(event) {
        alert( 'Lat: ' + event.latLng.lat() + ' and Longitude is: ' + event.latLng.lng() );
        });
        
        */
    }
    
             
</script>
  
   
  
</body>
</html>

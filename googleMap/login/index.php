<?php include("header.php") ?>
  <body>
  	<div class="container">
    	<h1 class="h1_signup">C-all</h1>

		<form method="POST" id="signupform" class="form_home">
 
			<?php
			if($error!=""){
						echo '<div class="alert alert-danger" role="alert">'.$error.'</div>' ;
				}
			?>

			<div class="form-group" >
			
				<input class="form-control"  type='text' id= 'email' placeholder="your email" name='email'></input>
			</div>
			<div class="form-group">
			
				<input class="form-control"  type="password" id= 'password' placeholder="your password" name='password'></input>
			</div>
            <div class="form-group">

                <input class="form-control"  type="text" id= 'userid' placeholder="userid" name='userid'></input>
            </div>
            <div class="form-group">

                <input class="form-control"  type="text" id= 'name' placeholder="Name " name='name'></input>
            </div>
            <div class="form-group">

                <input class="form-control"  type="text" id= 'mobile_no' placeholder="Mobile Number" name='mobile_no'></input>
            </div>
            <div class="form-group">

                <input class="form-control"  type="text" id= 'local' placeholder="local" name='local'></input>
            </div>
            <div class="form-group">

                <input class="form-control"  type="text" id= 'district' placeholder="district" name='district'></input>
            </div>
            <div class="form-group">

                <input class="form-control addressinput"  type="text" id= 'city' placeholder="city" name='city'></input>
             
                <input class="form-control addressinput"  type="text" id= 'state' placeholder="state" name='state'></input>
             
                <input class="form-control addressinput"  type="text" id= 'country' placeholder="country" name='country'></input>
            </div>

			<div class="form-check">
			    <label class="form-check-label">
					<input type="checkbox" class="form-check-input" id="stayLoggedIn" name='stayLoggedIn' value=1 >Stay Logged in
			    </label>
		  	</div> 
			<div class="form-group">
				<input type="hidden" name="signup"  value="1">
				<button id="submit" name="submit" class="btn btn-primary" > sign up</button>
			</div>

			<div class="toogle"><a href="#loginform"  >Login</a></div>

		</form> 

		<form method="POST" id="loginform"  class="form_home">
			 
			<?php if($error!=""){
						echo '<div class="alert alert-danger" role="alert">'.$error.'</div>' ;
				}
			?>
			 
			<div class="form-group">
				
				<input type='text' id= 'email' class="form-control"  placeholder="your email" name='email'></input>
			</div>
			<div class="form-group">
		
				<input type="password"  class="form-control"  id= 'password' placeholder="your password" name='password'></input>
			</div>
			<div class="form-check">
			    <label class="form-check-label">
					<input type="checkbox" class="form-check-input" id="stayLoggedIn" name='stayLoggedIn' value=1 >Stay Logged in
			    </label>
		  	</div> 
		  	<div class="form-group">
				<input type="hidden" name="signup"  value="0">
				<button id="submit" name="submit"  class="btn btn-primary" > log in</button>
			</div>
			<div class="toogle"><a href="#signupform"  >Signup</a></div>

		</form> 
	</div>



	<?php include("footer.php") ?>

	</body>

</html>
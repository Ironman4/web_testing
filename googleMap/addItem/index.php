<?php
    include("../login/connection.php");
    include ("connection_wall.php");
    session_start();
    $item_shown=0;

	if(array_key_exists("userid", $_COOKIE)  ){
        $_SESSION['userid'] = $_COOKIE["userid"] ;
        //echo "<a href='index.php?logout=1'>Logout</a>";

        // }else if(array_key_exists("id", $_SESSION) AND $_SESSION['id']!=0){
        // 	 //echo "<a href='index.php?logout=1'>Logout</a>";
    }else if(!array_key_exists("userid", $_SESSION) AND $_SESSION['userid']==0){

        header("Location:../login/index.php");
    }

    //welcome_bar setting var with user
    $welcome_bar ="Hello ".$_SESSION['userid'] ;

?>



<!doctype html>
<html lang="en" xmlns:color="http://www.w3.org/1999/xhtml" xmlns:border-radius="http://www.w3.org/1999/xhtml"
      xmlns:padding="http://www.w3.org/1999/xhtml">
<head>
    <title>C-all</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
</head>
<body>


<?php include ("../login/header.php");?>

<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top up" style="background-color: blue;">
        <a class="navbar-brand" href="#" id="brand" >C-all</a>
       <!-- <a class="navbar-brand" href="#" >Dashboard</a>  -->
        <span class="navbar-brand"  > <?php  echo $welcome_bar?></span>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse up" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
            </ul>
            <form class="form-inline mt-2 mt-md-0" style="position: relative;bottom: 8px">
                <input class="form-control mr-sm-2" type="text" data-action="grow" placeholder="Search Item within your account" aria-label="Search" id="itemSearch" >

                
                <button class="btn btn btn-success my-2 my-sm-0" type="submit">Search</button>
                <div class=" mr-sm-2">
                    <a href='../index.php?logout=1' id="bar" class="col-sm">
                        <button style="align-text:right" type="button" class="btn btn-success"> Logout
                        </button>
                    </a>
                </div>
            </form>

        </div>
    </nav>
</header>



<div class="back_layout container-fluid" >
    <form  action="additem.php" method="POST" enctype="multipart/form-data">

        <div class="form-row">
            <div class="form-group col-md-6">

                <input type="text" class="form-control" id="Item" name="item_name" placeholder="Item name">
            </div>

            <div class="form-group col-md-2">

                <input type="text" class="form-control" id="Cost" name="cost" placeholder="Cost of the item">
            </div>
            <div class="form-group col-md-4">

                <select id="inputState" class="form-control">
                    <option>INR</option>
                </select>
            </div>

        </div>
        <div class=" form-group">
            <!-- <div>
                <label class="custom-file">
                    <input type="file" id="image" class="custom-file-input" name="image">
                    <span class="custom-file-control"></span>
                </label>
            </div> -->
            <div>
                <label for="Description">Description</label>
                <textarea class="form-control" id="Description" rows="3" name="description"></textarea>
            </div>
            <div    >
            <button type="submit" class="btn btn-primary" style="margin-top: 30px">Add</button>
            </div>
        </div>
    </form>

</div>

<div class="container-fluid alt_back_layout">

    <?php
    include ("cardcontent.php");

    getCardItem($item_shown,$wallLink);
    ?>
    <div class="test">

    </div>
    <hr>
    <?php
        echo "<div class= 'item_shown' value='".$item_shown ."'> Total items are :$item_shown<div>";
    ?>
    <hr>

</div>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>

    //for the cart animation
//    $(".card").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){
//        $(".card").css({'position ': 'relative' ,  'width': '110%' , 'height': '103%'}) ;
//    });

//loading data without page reload
    function detectEndOfPage() {
        $(window).on("scroll", function() {
            var scrollHeight = $(document).height();
            var scrollPosition = $(window).height() + $(window).scrollTop();
            if ((scrollHeight - scrollPosition) / scrollHeight === 0) {

                alert($(".item_shown").attribute("value"));

                $.ajax({
                        url: "loadUserItems.php",
                        success: function (result) {
                            $(".test").html(result);
                        },
                        error: function () {
                            alert("failure");
                        }
                    });
            }
        });
    }


    $(document).on('focus', '[data-action="grow"]', function () {
      if ($(window).width() > 1000) {
        $(this).animate({
          width: 650
        })
      }
    })

    $(document).on('blur', '[data-action="grow"]', function () {
      if ($(window).width() > 1000) {
        var $this = $(this).animate({
          width: 180
        })
      }
    })
    

</script>

</body>
</html>
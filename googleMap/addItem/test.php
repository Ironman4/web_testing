<!DOCTYPE html>
<html>
<head>
    <style>
        #map {
            height: 400px;
            width: 100%;
        }
    </style>
</head>
<body>
<h3>My Google Maps Demo</h3>
<form>
    <input type="text" name="location" placeholder="Location">

</form>
<button id="search">search</button>
<div id="map" style="display: none;"></div>



<script>
    function initMap() {
        var uluru = {lat: -25.363, lng: 131.044};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCMlEaUR9y-N36-k06Q2zdui1LdNBkZ_A0&callback=initMap">
</script>
</body>
</html>
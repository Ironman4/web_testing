<?php
/**
 * Created by PhpStorm.
 * User: mabrin
 * Date: 15/12/17
 * Time: 5:32 AM
 */



function createCard ($row ){
    $item_name = $row["item"];
    $cost = $row["cost"];
    $unit =$row['unit'];
    $description = $row['description'];
    $image = $row["image"];
    $GLOBALS['item_shown']= $GLOBALS['item_shown'] +1 ;

    $card_data = <<<EOT
            <div class="col-sm-6 ">
                <div class="card meadium-width shadow" >
                    <div class="card-body">
                        <h4 class="card-title"><em>$item_name</em></h4>
                         
                        <p class="card-text">$description</p>
                        <div class="row">
                            <div> $cost</div>
                            <div>$unit</div>
                        </div>
                    </div>
                </div>
            </div>

EOT;
    return $card_data ;
}
//loading first data

function getCardItem($item_shown ,$wallLink)
{

    // $query = 'SELECT * FROM `' . mysqli_real_escape_string($wallLink, $_SESSION['userid']) . '`ORDER BY `item_num` DESC LIMIT ' . $item_shown . ', 4 ';
     $query = 'SELECT * FROM `' . mysqli_real_escape_string($wallLink, $_SESSION['userid']) . '`ORDER BY `item_num` DESC ';
    $result = mysqli_query($wallLink, $query);


    if (mysqli_num_rows($result) < 1) {

        $error = <<<EOT
            <div class="alert alert-danger" role="alert">
                You have no Item .
            </div>
EOT;


        echo $error;

    }
    while ($row = mysqli_fetch_assoc($result)) {
        $card_data = createCard($row);

        //joinign two card in one row
        if ($row = mysqli_fetch_assoc($result)) {

            $card_data = '<div class="row">' . $card_data . createCard($row) . '</div>';
        }
        echo $card_data;
    }
    return $item_shown ;
}

?>
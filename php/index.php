
<?php

  $weather="";
  $error="";
  if(array_key_exists('city', $_GET))  {

  $city  = str_replace(' ', '', $_GET["city"]);
  $file_headers = @get_headers("https://www.weather-forecast.com/locations/".$city."/forecasts/latest");
  if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
      $error = "Page not found";
  

  }else{
  $page = file_get_contents("https://www.weather-forecast.com/locations/".$city."/forecasts/latest");
  


  $pagefirst = explode('3 Day Weather Forecast Summary:</b><span class="read-more-small"><span class="read-more-content"> <span class="phrase">',$page);
  
  $pagesecond= explode('</span></span></span>',$pagefirst[1]);
  
  $weather = $pagesecond[0];
}
}else{
  $error = "Page not found";
}


?>

<!doctype html>
<html lang="en">
  <head>
    <title>weather scrapper </title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">


    <style type="text/css">
      html { 
      background: url(background.jpg) no-repeat center center fixed; 
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
     }
     body{
      background : none ;
    
     }
      
     .container{
      width: 450px;
      text-align: center;
      margin-top: 150px;

     }

     input{
      margin-top: 20px;

     }
     button{
      margin-bottom: 20px;
     }
    </style>

  </head>
  <body>
    <div class="container">
    <h1>What is the weather !</h1>
    <form>
      <div class="form-group">
        <label for="city">Enter the name of the city</label>
        <input type="text" class="form-control" id="city" aria-describedby="emailHelp" placeholder="eg. London , Tokyo etc." name="city" value='<?php if(array_key_exists("city", $_GET)){echo "$_GET[city]";} ?>'>
         
      </div>
     
      <button type="submit" class="btn btn-primary">Submit</button>
  </form>
      <?php if($weather){
          echo '<div class="alert alert-success" role="alert">'.$weather.'</div>' ;
        }else{
        echo '<div class="alert alert-danger" role="alert">'.$error.'</div>';
      }
       ?>
       
  </div>

    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </body>
</html>